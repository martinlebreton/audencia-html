
/* VARIABLES*/
var parts = 4; /*quantité de pers.*/
var crepes = 5; /* quantité de crêpes pour 1 pers*/

var affichagePers = document.getElementById("personnes");
var affichageCompteur = document.getElementById("compteur");
var farine = document.getElementById("farine");
var oeufs = document.getElementById("oeufs");
var sucre = document.getElementById("sucre");
var beurre = document.getElementById("beurre");
var lait = document.getElementById("lait");
var rhum = document.getElementById("rhum");

var partFarine = 100;
var partOeufs = 1;
var partSucre = 1;
var partBeurre = 15;
var partLait = 20;
var partRhum = 1;


/* FONCTIONS */

function maj(){
  //mise à jour des proportions d'après le nombre de parts
  farine.textContent = partFarine*parts;
  oeufs.textContent = partOeufs*parts;
  sucre.textContent = partSucre*parts;
  beurre.textContent = partBeurre*parts;
  lait.textContent = partLait*parts;
  rhum.textContent = partRhum*parts;
}


function setCompteur(){
  //mise à jour du nombre de crêpes et de personnes
  affichageCompteur.textContent = parts*crepes;
  affichagePers.textContent = parts;
}


function augmenterProportion(){
  // incrementation du nombre de parts et mise à jour
  parts += 1;
  if(parts>9){parts = 9}
  setCompteur (parts);
  maj();
}


function reduireProportion(){
  // décrementation du nombre de parts et mise à jour
  parts -= 1;
  if(parts<3){parts = 3}
  setCompteur (parts);
  maj();
}


/* INITIALISATION */
window.onload = setCompteur();
window.onload = maj();
