


# Partie Javascript

Objectif être capable de créer variables, alert, test, getElementById

```Javascript

/* VARIABLES*/

var message = "Bonjour";
var a = 3 + 2;
var b = 10


/* messages */

alert(message);

alert(a);

if( a < b){
  alert("a est égal à " + a + ", il est plus petit que b qui est égal à " + b);
}else {
  alert("a est plus grand que b");
}

```
et ensuite sélectionner et modifier le DOM

```Javascript


var affichageCompteur = document.getElementById("compteur");

affichageCompteur.textContent = "25";


```
