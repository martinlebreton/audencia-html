# Initiation HTML/CSS

Cette page documente le projet et vous donne accès aux ressources.


## Outils

Pour travailler et "coder" nous allons utiliser une éditeur de code, il en existe plusieurs, nous allons choisir Brackets :
[Brackets](http://brackets.io/)

![brackets-icon](Ressources/brackets-logo.png)

* Téléchargez
* Installez
* Créez un dossier "audencia-html" sur votre ordinateur
* Lancez Brackets
* "Fichier > Ouvrir un dossier" et retrouvez votre dossier "audencia-html"

## Projet

Voici le projet que nous allons réaliser ensemble, il s'agit d'une page de recette assez simple.

![Projet Html](Ressources/maquette.jpg)

![Projet Html](Ressources/zoning.jpg)

# Ressources

## Ressources pour la première partie : [dossier-depart.zip](Ressources/DOSSIER-DEPART.zip) (clic droit "enregistrer le lien sous...")

* Télécharger la ressource
* Dézipper la ressource
* Placer le dossier "dossier-depart" dans votre dossier "audencia-html"

## Ressources pour la seconde partie : [dossier-suite.zip](Ressources/DOSSIER-SUITE-JS.zip) (clic droit "enregistrer le lien sous...")

* Télécharger la ressource
* Dézipper la ressource
* Placer le dossier "dossier-suite-js" dans votre dossier "audencia-html"


# Documentations

* Documentation complète [Html](https://developer.mozilla.org/fr/docs/Web/HTML) par Mozilla

* Documentation complète [Css](https://developer.mozilla.org/fr/docs/Web/CSS) par Mozilla

## Aller plus loin

* Documentation sur les [bases du référencement](https://support.google.com/webmasters/answer/7451184?hl=fr) par Google

* Documentation sur les bonnes pratiques du référencement avec une [Checklist SEO](https://fr.semrush.com/blog/checklist-seo/) par SEMRush

* Cours et certification au marketing digital avec les [atelier numériques](https://learndigital.withgoogle.com/ateliersnumeriques/courses) de Google


Enfin le [dossier complet](Ressources/DOSSIER-FINAL.zip) du projet, à récupérer en fin de séance.
